/**
 * A library of package management commands which are used by various interfaces around the software.
 * @extends {Game}
 */
class Setup extends Game {

  /**
   * A reference to the setup URL used under the current route prefix, if any
   * @return {string}
   */
  static get setupURL() {
    return foundry.utils.getRoute("setup");
  }

  /* -------------------------------------------- */

  /**
   * Register core game settings
   * @override
   */
  registerSettings() {
    super.registerSettings();
    game.settings.register("core", "declinedManifestUpgrades", {
      scope: "client",
      config: false,
      type: Object,
      default: {}
    });
  }

  /* -------------------------------------------- */

  /** @override */
  setupPackages(data) {
    super.setupPackages(data);

    /**
     * The game World which is currently active
     * @type {Object}
     */
    this.worlds = new Map((data.worlds || []).map(m => {
      m.data = new foundry.packages.WorldData(m.data);
      return [m.id, m]
    }));

    /**
     * The System which is used to power this game world
     * @type {Object}
     */
    this.systems = new Map((data.systems || []).map(m => {
      m.data = new foundry.packages.SystemData(m.data);
      return [m.id, m]
    }));
  }

  /* -------------------------------------------- */

  /** @override */
  static async getData(socket, view) {
    let req;
    switch (view) {
      case "auth": case "license": req = "getAuthData"; break;
      case "join": req = "getJoinData"; break;
      case "players": req = "getPlayersData"; break;
      case "setup": req = "getSetupData"; break;
    }
    return new Promise(resolve => {
      socket.emit(req, resolve);
    });
  }

  /* -------------------------------------------- */
  /*  View Handlers                               */
  /* -------------------------------------------- */

  /** @override */
  async _initializeView() {
    switch (this.view) {
      case "auth":
        return this._authView();
      case "license":
        return this._licenseView();
      case "setup":
        return this._setupView();
      case "players":
        return this._playersView();
      case "join":
        return this._joinView();
      default:
        throw new Error(`Unknown view URL ${this.view} provided`);
    }
  }

  /* -------------------------------------------- */

  /**
   * The application view which displays the End User License Agreement (EULA).
   * @private
   */
  _licenseView() {
    ui.notifications = new Notifications().render(true);
    const setup = document.getElementById("setup");
    if ( setup.dataset.step === "eula" ) new EULA().render(true);
  }

  /* -------------------------------------------- */

  /**
   * The application view which displays the admin authentication application.
   * @private
   */
  _authView() {
    if ( !SIGNED_EULA ) window.location.href = foundry.utils.getRoute("license");
    ui.notifications = new Notifications().render(true);
    new SetupAuthenticationForm().render(true);
  }

  /* -------------------------------------------- */

  /**
   * The application view which displays the application Setup and Configuration.
   * @private
   */
  _setupView() {
    if ( !SIGNED_EULA ) window.location.href = foundry.utils.getRoute("license");
    ui.notifications = new Notifications().render(true);
    ui.setup = new SetupConfigurationForm(game.data).render(true);
    Setup._activateSocketListeners();
  }

  /* -------------------------------------------- */

  /**
   * The application view which displays the User Configuration.
   * @private
   */
  _playersView() {
    if ( !SIGNED_EULA ) window.location.href = foundry.utils.getRoute("license");
    this.users = new Users(this.data.users);
    this.collections.set("User", this.users);
    this.collections.set("Setting", this.settings.storage.get("world"));

    // Render applications
    ui.notifications = new Notifications().render(true);
    ui.players = new UserManagement(this.users);
    ui.players.render(true);

    // Game is ready for use
    this.ready = true;
  }

  /* -------------------------------------------- */

  /**
   * The application view which displays the Game join and authentication screen.
   * @private
   */
  _joinView() {
    if ( !SIGNED_EULA ) window.location.href = foundry.utils.getRoute("license");

    // Configure Join view data
    this.users = new Users(this.data.users);
    this.collections.set("User", this.users);

    // Activate Join view socket listeners
    Users._activateSocketListeners(this.socket);

    // Render Join view applications
    ui.notifications = new Notifications().render(true);
    ui.join = new JoinGameForm().render(true);
  }

  /* -------------------------------------------- */
  /*  Package Management                          */
  /* -------------------------------------------- */

  /**
   * Check with the server whether a package of a certain type is able to be installed or updated.
   * @param {string} type       The package type to check
   * @param {string} name       The package name to check
   * @param {string} manifest   The manifest URL to check
   * @param {number} timeout    A timeout in milliseconds after which the check will fail
   * @return {Promise<Object>}  The return manifest
   */
  static async checkPackage({type="module", name, manifest, timeout=20000}={}) {
    return this.post({action: "checkPackage", type, name, manifest}, timeout).then(r => r.json());
  }

  /* -------------------------------------------- */

  /**
   * Prepares the cache of available and owned packages
   * @param type
   * @return {Promise<void>}
   */
  static async warmPackages({type="system"}={}) {
    if ( Setup.cache[type].state > Setup.CACHE_STATES.COLD ) return;
    Setup.cache[type].state = Setup.CACHE_STATES.WARMING;
    const result = await this.getPackages({type});
    // If we fail to get packages, there's no point asking for Owned
    if ( result.size > 0 ) await this.getOwned({type});
    Setup.cache[type].state = Setup.CACHE_STATES.WARMED;
  }

  /* -------------------------------------------- */

  /**
   * Get a Map of available packages of a given type which may be installed
   * @param {string} type
   * @return {Promise<Map<string, Package>>}
   */
  static async getPackages({type="system"}={}) {
    if ( this.cache[type].packages?.size > 0 ) return this.cache[type].packages;
    const packages = new Map();
    let request;
    try {
      request = await this.post({action: "getPackages", type: type});
    } catch(err) {
      ui.notifications.error("PACKAGE.GetPackagesTimedOut", {localize: true});
      return packages;
    }
    if ( !request.ok ) return packages;
    let response = await request.json();
    response.packages.forEach(p => packages.set(p[0], p[1]));
    this.cache[type].packages = packages;
    return packages;
  }

  /* -------------------------------------------- */

  /**
   * Get a map of owned packages of a given type
   * @param {string} type
   * @return {Promise<Map<string, Package>>}
   */
  static async getOwned({type="system"}={}) {
    if ( this.cache[type].owned?.size > 0 ) return this.cache[type].owned;
    let request;
    try {
      request = await this.post({action: "getPackages", type: type});
    } catch(err) {
      ui.notifications.error("PACKAGE.WarmCacheTimedOut", {localize: true});
      return new Map();
    }
    const response = await request.json();
    this.cache[type].owned = response.owned;
    return response.owned;
  }

  /* -------------------------------------------- */

  /**
   * Install a Package
   * @param {string} type          The type of package being installed, in ["module", "system", "world"]
   * @param {string} name          The canonical package name
   * @param {string} manifest      The package manifest URL
   * @param {function} onProgress  A function that will receive progress updates during the installation process
   * @return {Promise<Object>}     A Promise which resolves to the installed package
   */
  static installPackage = ({type="module", name, manifest}={}, onProgress) => new Promise(async resolve => {
    const error = response => {
      const err = new Error(response.error);
      err.stack = response.stack;
      ui.notifications.error(game.i18n.format("SETUP.InstallFailure", {message: err.message}));
      console.error(err);
      Setup._removeProgressListener(progress);
      if ( onProgress ) Setup._removeProgressListener(onProgress);
      resolve(response);
    };

    const done = async ({pkg}) => {
      ui.notifications.info(game.i18n.format("SETUP.InstallSuccess", {type: type.titleCase(), name: pkg.data.name}));

      // Trigger dependency installation (asynchronously)
      if ( pkg.data.dependencies?.length ) {
        // noinspection ES6MissingAwait
        this.installDependencies(pkg);
      }

      // Update in-memory data
      game[`${type}s`].set(pkg.data.name, pkg);

      // Update application views
      if ( ui.setup ) await ui.setup.reload();
      Setup._removeProgressListener(progress);
      if ( onProgress ) Setup._removeProgressListener(onProgress);
      resolve(pkg);
    };

    const progress = data => {
      if ( data.step === "Error" ) return error(data);
      if ( data.step === "Package" ) return done(data);
    };

    Setup._addProgressListener(progress);
    if ( onProgress ) Setup._addProgressListener(onProgress);
    let request;
    try {
      request = await this.post({action: "installPackage", type, name, manifest});
    } catch(err) {
      ui.notifications.error("PACKAGE.PackageInstallTimedOut", {localize: true});
      resolve();
      return;
    }

    /** @type {InstallPackageResponse} */
    const response = await request.json();

    // Handle errors
    if ( response.error ) error(response);
    // Handle warnings
    if ( response.warning ) ui.notifications.warn(response.warning);
  });

  /* -------------------------------------------- */

  /**
   * Install a set of dependency modules which are required by an installed package
   * @param {InstallPackageResponse} pkg   The package which was installed that requested dependencies
   * @return {Promise<void>}
   */
  static async installDependencies(pkg) {
    const toInstall = [];
    const dependencies = pkg.data.dependencies || [];

    // Obtain known package data for requested dependency types
    for ( let d of dependencies ) {
      if (!d.name) continue;
      d.type = d.type || "module";
      const installed = game.data[`${d.type}s`].find(p => p.id === d.name);
      if ( installed ) {
        console.debug(`Dependency ${d.type} ${d.name} is already installed.`);
        continue;
      }

      // Manifest URL provided
      if (d.manifest) {
        toInstall.push(d);
        continue;
      }

      // Discover from package listing
      const packages = await Setup.getPackages({type: d.type});
      const dep = packages.get(d.name);
      if (!dep) {
        console.warn(`Requested dependency ${d.name} not found in ${d.type} directory.`);
        continue;
      }
      d.manifest = dep.version.manifest;
      d.version = dep.version.version;
      toInstall.push(d);
    }
    if ( !toInstall.length ) return;

    // Prompt the user to confirm installation of dependency packages
    const html = await renderTemplate("templates/setup/install-dependencies.html", {
      title: pkg.data.title,
      dependencies: toInstall
    });
    let agree = false;
    await Dialog.confirm({
      title: game.i18n.localize("SETUP.PackageDependenciesTitle"),
      content: html,
      yes: () => agree = true
    });
    if ( !agree ) return ui.notifications.warn(game.i18n.format("SETUP.PackageDependenciesDecline", {
      title: pkg.data.title
    }));

    // Install dependency packages
    for ( let d of toInstall ) {
      await this.installPackage(d);
    }
    return ui.notifications.info(game.i18n.format("SETUP.PackageDependenciesSuccess", {
      title: pkg.data.title,
      number: toInstall.length
    }));
  }

  /* -------------------------------------------- */

  /**
   * Uninstall a single Package by name and type.
   * @param {string} type       The type of package being installed, in ["module", "system", "world"]
   * @param {string} name       The canonical package name
   * @return {Promise<object>}  A Promise which resolves to the uninstalled package manifest
   */
  static async uninstallPackage({type="module", name}={}) {
    let request;
    try {
      request = await this.post({action: "uninstallPackage", type, name});
    } catch(err) {
      return {error: err.message, stack: err.stack};
    }
    // Update in-memory data
    game[`${type}s`].delete(name);
    return request.json();
  }

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers               */
  /* -------------------------------------------- */

  /**
   * Activate socket listeners related to Setup
   */
  static _activateSocketListeners() {
    game.socket.on("progress", Setup._onProgress);
  }

  /* --------------------------------------------- */

  /**
   * A list of functions to call on progress events.
   * @type {function[]}
   */
  static _progressListeners = [];

  /* --------------------------------------------- */

  /**
   * Handle a progress event from the server.
   * @param {Object} data  The progress update data.
   * @private
   */
  static _onProgress(data) {
    Setup._progressListeners.forEach(l => l(data));
  }

  /* --------------------------------------------- */

  /**
   * Add a function to be called on a progress event.
   * @param {function} listener
   */
  static _addProgressListener(listener) {
    Setup._progressListeners.push(listener);
  }

  /* --------------------------------------------- */

  /**
   * Stop sending progress events to a given function.
   * @param {function} listener
   */
  static _removeProgressListener(listener) {
    Setup._progressListeners = Setup._progressListeners.filter(l => l !== listener);
  }

  /* -------------------------------------------- */
  /*  Helper Functions                            */
  /* -------------------------------------------- */

  /**
   * A helper method to submit a POST request to setup configuration with a certain body, returning the JSON response
   * @param {Object} body             The request body to submit
   * @param {number} [timeout=30000]  The time, in milliseconds, to wait before aborting the request
   * @return {Promise<Object>}        The response body
   * @private
   */
  static post(body, timeout = 30000) {
    if (!((game.view === "setup") || (game.ready && game.user.isGM && body.shutdown))) {
      throw new Error("You may not submit POST requests to the setup page while a game world is currently active.");
    }
    return fetchWithTimeout(this.setupURL, {
      method: "POST",
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(body),
      redirect: "manual"
    }, {timeoutMs: timeout});
  }
}

/**
 * An enum that indicates a state the Cache is in
 * @enum {number}
 */
Setup.CACHE_STATES = {
  COLD: 0,
  WARMING: 1,
  WARMED: 2
};


/**
 * A cached object of retrieved packages from the web server
 * @type {{world: object[], system: object[], module: object[]}}
 */
Setup.cache = {
  world: { packages: new Map(), state: Setup.CACHE_STATES.COLD },
  module: { packages: new Map(), state: Setup.CACHE_STATES.COLD },
  system: { packages: new Map(), state: Setup.CACHE_STATES.COLD }
};

/**
 * The End User License Agreement
 * Display the license agreement and prompt the user to agree before moving forwards
 * @type {Application}
 */
class EULA extends Application {
  /** @inheritdoc */
	static get defaultOptions() {
	  const options = super.defaultOptions;
	  options.id = "eula";
	  options.template = "templates/setup/eula.html";
	  options.title = "End User License Agreement";
	  options.width = 720;
	  options.popOut = true;
	  return options;
  }

  /* -------------------------------------------- */

  /**
   * A reference to the setup URL used under the current route prefix, if any
   * @return {string}
   */
  get licenseURL() {
    return getRoute("license");
  }

  /* -------------------------------------------- */

  /** @override */
  async getData() {
      let html = await fetchWithTimeout("license.html").then(r => r.text());
	  return {
        html: html
	  }
  }

  /* -------------------------------------------- */

  /** @override */
	async _renderOuter() {
	  const id = this.id;
	  const classes = Array.from(this.options.classes).join(" ");

	  // Override the normal window app wrapper so it cannot be closed or minimized
	  const parsed = $.parseHTML(`<div id="${id}" class="app window-app ${classes}" data-appid="${this.appId}">
      <header class="window-header flexrow">
          <h4 class="window-title">${this.title}</h4>
      </header>
      <section class="window-content"></section>
    </div>`);
	  const html = $(parsed[0]);

    // Make the outer window draggable
    const header = html.find('header')[0];
    new Draggable(this, html, header, this.options.resizable);

    // Set the outer frame z-index
    if ( Object.keys(ui.windows).length === 0 ) _maxZ = 100;
    html.css({zIndex: Math.min(++_maxZ, 9999)});
    return html;
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    const form = html[0].children[1];
    html.find("#decline").click(this._onDecline.bind(this));
    form.onsubmit = this._onSubmit.bind(this);
  }

  /* -------------------------------------------- */

  /**
   * Handle refusal of the EULA by checking the decline button
   * @param {MouseEvent} event    The originating click event
   */
  _onDecline(event) {
    const button = event.currentTarget;
    ui.notifications.error(`You have declined the End User License Agreement and cannot use the software.`);
    button.form.dataset.clicked = "decline";
  }

  /* -------------------------------------------- */

  /**
   * Validate form submission before sending it onwards to the server
   * @param {Event} event       The originating form submission event
   */
  _onSubmit(event) {
    const form = event.target;
    if ( form.dataset.clicked === "decline" ) {
      return setTimeout(() => window.location.href = CONST.WEBSITE_URL, 1000);
    }
    if ( !form.agree.checked ) {
      event.preventDefault();
      ui.notifications.error(`You must agree to the ${this.options.title} before proceeding.`);
    }
  }
}

/**
 * A special class of Dialog which allows for the installation of Packages.
 * @extends {Application}
 */
class InstallPackage extends Application {
  constructor(data, options) {
    super(options);
    this.data = data;

    /**
     * The instance of the setup form to which this is linked
     * @type {SetupConfigurationForm}
     */
    this.setup = data.setup;

    /**
     * The category being filtered for
     * @type {string}
     */
    this._category = "all";

    /**
     * The visibility being filtered for
     * @type {string}
     */
    this._visibility = "all";

    /**
     * The list of installable packages
     * @type {Array<Package>}
     */
    this.packages = undefined;

    /**
     * The list of Tags available
     * @type {Array<Object>}
     */
    this.tags = undefined;

    /**
     * Have we initialized the filter to a special value?
     * @type {boolean}
     * @private
     */
    this._initializedFilter = !this.data.filterValue;
  }

	/* -------------------------------------------- */

  /** @override */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
	    id: "install-package",
	    template: "templates/setup/install-package.html",
      classes: ["dialog"],
      width: 720,
      height: 620,
      scrollY: [".categories", ".package-list"],
      filters: [{inputSelector: 'input[name="filter"]', contentSelector: ".package-list"}]
    });
  }

	/* -------------------------------------------- */

  /** @override */
  get title() {
    return game.i18n.localize("SETUP.Install"+this.data.packageType.titleCase());
  }

	/* -------------------------------------------- */

  render(...args) {
    // Lazily load packages
    const type = this.data.packageType;
    if ( Setup.cache[type].state === Setup.CACHE_STATES.COLD ) {
      Setup.warmPackages({type}).then(() => this.render(false));
    }
    return super.render(...args);
  }

    /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    const data = super.getData(options);
    const type = data.packageType = this.data.packageType;
    if ( !this.packages?.length || !this.tags?.length ) {
      const {packages, tags} = await InstallPackage.getTaggedPackages(type);
      this.packages = packages;
      this.tags = tags;
    }
    data.loading = Setup.cache[type].state < Setup.CACHE_STATES.WARMED;
    data.couldntLoad = !this.packages.length && Setup.cache[type].state == Setup.CACHE_STATES.WARMED;

    // Category filters
    data.tags = Object.entries(this.tags).reduce((tags, t) => {
      let [k, v] = t;
      v.active = this._category === t[0];
      v.css = t[1].active ? " active" : "";
      tags[k] = v;
      return tags;
    }, {});

    // Visibility filters
    data.visibility = [
      { id: "inst", css: this._visibility === "inst" ? " active" : "", label: "SETUP.PackageVisInst" },
      { id: "unin", css: this._visibility === "unin" ? " active" : "", label: "SETUP.PackageVisUnin" },
      { id: "all", css: this._visibility === "all" ? " active" : "", label: "SETUP.PackageVisAll" }
    ];

    // Filter packages
    const installed = new Set(game.data[`${type}s`].map(s => s.id));
    data.packages = this.packages.filter(p => {
      p.installed = installed.has(p.name);
      if ( (this._visibility === "unin") && p.installed ) return false;
      if ( (this._visibility === "inst") && !p.installed ) return false;
      p.cssClass = [p.installed ? "installed" : null, p.installable ? null: "locked"].filterJoin(" ");
      if ( this._category === "all" ) return true;
      if ( this._category === "premium" ) return p.is_protected;
      if ( this._category === "exclusive" ) return p.is_exclusive;
      return p.tags.includes(this._category);
    });
    return data;
  }

	/* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    html[0].children[0].onsubmit = ev => ev.preventDefault();
    html.find(".package-title a").click(this._onClickPackageTitle.bind(this));
    html.find("button.install").click(this._onClickPackageInstall.bind(this));
    html.find(".category .filter").click(this._onClickCategoryFilter.bind(this));
    html.find(".visibilities .visibility").click(this._onClickVisibilityFilter.bind(this));
    html.find("input[name=filter]").focus();

    const loading = Setup.cache[this.data.packageType].state < Setup.CACHE_STATES.WARMED;
    if ( !this._initializedFilter && !loading ) {
      html.find('input[name="filter"]').val(this.data.filterValue);
      this._searchFilters[0].filter(null, this.data.filterValue);
      this._initializedFilter = true;
    }
  }

	/* -------------------------------------------- */

  /**
   * Handle left-click events to filter to a certain module category
   * @param {MouseEvent} event
   * @private
   */
  _onClickCategoryFilter(event) {
    event.preventDefault();
    this._category = event.target.dataset.category || "all";
    this.render();
  }

	/* -------------------------------------------- */

  /**
   * Handle left-click events to filter to a certain visibility state
   * @param {MouseEvent} event
   * @private
   */
  _onClickVisibilityFilter(event) {
    event.preventDefault();
    this._visibility = event.target.dataset.visibility || "all";
    this.render();
  }

  /* -------------------------------------------- */

  /**
   * Handle a left-click event on the package title
   * @param {MouseEvent} event
   * @private
   */
  _onClickPackageTitle(event) {
    event.preventDefault();
    const li = event.currentTarget.closest(".package");
    if ( li.classList.contains("installed") ) return;
    if ( li.classList.contains("locked") ) {
      const href = `https://foundryvtt.com/packages/${li.dataset.packageId}/`;
      return window.open(href, "_blank");
    }
    const form = li.closest("form");
    form.manifestURL.value = li.querySelector("button.install").dataset.manifest;
  }

	/* -------------------------------------------- */

  /**
   * Handle a left-click event on the package "Install" button
   * @param {MouseEvent} event
   * @private
   */
  async _onClickPackageInstall(event) {
    event.preventDefault();
    const button = event.currentTarget;
    button.disabled = true;
    const type = this.data.packageType;
    let manifest = button.dataset.manifest;

    // Install from manifest field
    if (button.dataset.action === "install-url") {
      manifest = button.form.manifestURL.value.trim();
    }

    // Install from package listing
    else {
      const li = button.closest(".package");
      if ( li.classList.contains("locked") ) {
        const href = `https://foundryvtt.com/packages/${li.dataset.packageId}/`;
        return window.open(href, "_blank");
      }
    }

    // Execute the installation
    await Setup.installPackage({type, manifest}, data => {
      this.setup.updateProgressBar(data);
      this.setup.updateProgressButton(data);
    });
    button.disabled = false;
  }

	/* -------------------------------------------- */

  /** @override */
  _onSearchFilter(event, query, rgx, html) {
    for ( let li of html.children ) {
      if ( !query ) {
        li.classList.remove("hidden");
        continue;
      }
      const id = li.dataset.packageId;
      const title = li.querySelector(".package-title a")?.textContent;
      const author = li.querySelector(".tag.author").textContent;
      const match = rgx.test(SearchFilter.cleanQuery(id)) ||
        rgx.test(SearchFilter.cleanQuery(title)) ||
        rgx.test(SearchFilter.cleanQuery(author));
      li.classList.toggle("hidden", !match);
    }
  }

	/* -------------------------------------------- */

  /**
   * Organize package data and cache it to the application
   * @param {string} type   The type of packages being retrieved
   * @return {object[]}     The retrieved or cached packages
   */
  static async getTaggedPackages(type) {
    // Identify package tags and counts
    const packages = [];
    const ownedPackages = Setup.cache[type].owned;
    const counts = {premium: 0, exclusive: 0};
    const unordered_tags = {};
    for ( let pack of Setup.cache[type].packages.values() ) {
      const owned = pack.is_protected && ownedPackages.includes(pack.id);
      const p = {
        id: pack.id,
        name: pack.name,
        title: pack.title,
        requires: pack.requires,
        cssClass: pack.cssClass,
        description: pack.description,
        url: pack.url,
        author: pack.author,
        version: pack.version,
        installed: pack.installed,
        is_exclusive: pack.is_exclusive,
        is_protected: pack.is_protected,
        owned: owned,
        installable: pack.is_protected ? owned : true,
        tags: pack.tags.map(t => {
          const [k, v] = t;
          if ( !unordered_tags[k] ) unordered_tags[k] = {label: v, count: 0, [type]: true};
          unordered_tags[k].count++;
          return k;
        })
      };
      if ( pack.is_exclusive ) counts.exclusive++;
      if ( pack.is_protected ) counts.premium++;

      packages.push(p);
    }

    // Organize category tags
    const sorted_tags = Array.from(Object.keys(unordered_tags));
    sorted_tags.sort();
    const tags = sorted_tags.reduce((obj, k) => {
      obj[k] = unordered_tags[k];
      return obj;
    }, {
      all: { label: "All Packages", count: packages.length, [type]: true},
      premium: { label: "Premium Content", count: counts.premium, [type]: true},
      exclusive: { label: "Exclusive Content", count: counts.exclusive, [type]: true }
    });
    return { packages: packages, tags: tags };
  }
}

/**
 * The Join Game setup application
 * @extends {FormApplication}
 */
class JoinGameForm extends FormApplication {
  constructor(object, options) {
    super(object, options);
    game.users.apps.push(this);
  }

	/* -------------------------------------------- */

  /** @override */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
	    id: "join-game",
      template: "templates/setup/join-game.html",
      popOut: false,
      closeOnSubmit: false,
      scrollY: ["#world-description"]
    });
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    const data = {
      isAdmin: game.data.isAdmin,
      users: game.users,
      world: game.world.data,
      passwordString: game.data.passwordString,
      usersCurrent: game.users.filter(u => u.active).length,
      usersMax: game.users.contents.length,
    };

    // Next session time
    const nextDate = new Date(game.world.data?.nextSession || undefined);
    if ( nextDate.isValid() ) {
      data.nextDate = nextDate.toDateInputString();
      data.nextTime = nextDate.toTimeInputString();
      const fmt = new Intl.DateTimeFormat(undefined, {timeZoneName: "short"});
      const tz = fmt.formatToParts().find(p => p.type === "timeZoneName");
      data.nextTZ = tz ? ` (${tz.value})` : "";
    }
    return data;
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);
    this.form.userid.addEventListener("focus", ev => this._setMode("join"));
    this.form.password.addEventListener("focus", ev => this._setMode("join"));
    this.form.adminKey?.addEventListener("focus", ev => this._setMode("shutdown"));
    this.form.shutdown.addEventListener("click", this._onShutdown.bind(this));
  }

	/* -------------------------------------------- */

  /**
   * Toggle the submission mode of the form to alter what pressing the "ENTER" key will do
   * @param {string} mode
   * @private
   */
  _setMode(mode) {
    switch (mode) {
      case "join":
        this.form.shutdown.type = "button";
        this.form.join.type = "submit";
        break;
      case "shutdown":
        this.form.join.type = "button";
        this.form.shutdown.type = "submit";
        break;
    }
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  async _onSubmit(event, options) {
    event.preventDefault();
    const form = event.target;
    form.submit.disabled = true;
    const data = this._getSubmitData();
    data.action = "join";
    return this._post(data, form.submit);
  }

	/* -------------------------------------------- */

  /**
   * Handle requests to shut down the currently active world
   * @param {MouseEvent} event    The originating click event
   * @returns {Promise<void>}
   * @private
   */
  async _onShutdown(event) {
    event.preventDefault();
    const button = event.currentTarget;
    button.disabled = true;
    const data = this._getSubmitData();
    data.action = "shutdown";
    return this._post(data, button);
  }

	/* -------------------------------------------- */

  /**
   * Submit join view POST requests to the server for handling.
   * @param {object} formData                         The processed form data
   * @param {EventTarget|HTMLButtonElement} button    The triggering button element
   * @returns {Promise<void>}
   * @private
   */
  async _post(formData, button) {
    const joinURL = foundry.utils.getRoute("join");
    button.disabled = true;

    // Look up some data
    const user = game.users.get(formData.userid)?.name || formData.userid;

    let response;
    try {
      response = await fetchJsonWithTimeout(joinURL, {
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(formData)
      });
    }
    catch (e) {
      if (e instanceof HttpError) {
        const error = game.i18n.format(e.displayMessage, {user});
        ui.notifications.error(error);
      }
      else {
        ui.notifications.error(e);
      }
      button.disabled = false;
      return;
    }

    // Redirect on success
    ui.notifications.info(game.i18n.format(response.message, {user}));
    setTimeout(() => window.location.href = response.redirect, 500 );
  }
}

/**
 * The Setup Authentication Form
 * @extends {Application}
 */
class SetupAuthenticationForm extends Application {
  /** @inheritdoc */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
	    id: "setup-authentication",
      template: "templates/setup/setup-authentication.html",
      popOut: false
    });
  }
}

/**
 * The Package Configuration setup application
 * @extends {Application}
 */
class SetupConfigurationForm extends FormApplication {
  constructor({systems, modules, worlds}={}) {
    super({});

    /**
     * Valid Game Systems to choose from
     * @type {object[]}
     */
    this.systems = systems;

    /**
     * Install Modules to configure
     * @type {object[]}
     */
    this.modules = modules;

    /**
     * The Array of available Worlds to load
     * @type {object[]}
     */
    this.worlds = worlds;

    /**
     * The currently viewed tab
     * @type {string}
     */
    this._tab = "worlds";

    /**
     * Track the button elements which represent updates for different named packages
     * @type {HTMLElement|null}
     */
    this._progressButton = null;

    /**
     * Keeps track of which packages were updated to enable displaying their state on redraw
     * @type {Set<string>}
     * @private
     */
    this._updatedPackages = new Set();
  }

	/* -------------------------------------------- */

  /** @override */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
	    id: "setup-configuration",
      classes: ["dark"],
      template: "templates/setup/setup-config.html",
      popOut: false,
      scrollY: ["#world-list", "#system-list", "#module-list"],
      tabs: [{navSelector: ".tabs", contentSelector: ".content", initial: "worlds"}],
      filters: [
        {inputSelector: '#world-filter', contentSelector: "#world-list"},
        {inputSelector: '#system-filter', contentSelector: "#system-list"},
        {inputSelector: '#module-filter', contentSelector: "#module-list"}
      ]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  _onSearchFilter(event, query, rgx, html) {
    let anyMatch = !query;
    for ( let li of html.children ) {
      if ( !query ) {
        li.classList.remove("hidden");
        continue;
      }
      const id = li.dataset.packageId;
      const title = li.querySelector(".package-title")?.textContent;
      const match = rgx.test(id) || rgx.test(SearchFilter.cleanQuery(title));
      li.classList.toggle("hidden", !match);
      if ( match ) anyMatch = true;
    }
    const empty = !anyMatch || !html.children.length;
    html.classList.toggle("empty", empty);
    html.previousElementSibling.classList.toggle("hidden", anyMatch);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onChangeTab(event, tabs, active) {
    super._onChangeTab(event, tabs, active);
    // Clear the search filter.
    this._searchFilters.forEach(f => {
      if ( f._input ) f._input.value = "";
      f.filter(null, "");
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {

    // Configuration options
    options = game.data.options;
    options.upnp = options.upnp !== false;

    // Prepare Systems
    const systems = this.systems.map(s => {
      foundry.packages.tagPackageAvailability(s);
      s.data.updated = this._updatedPackages.has(s.id);
      return s;
    }).sort((a, b) => a.data.title.localeCompare(b.data.title));

    // Prepare Modules
    const modules = this.modules.map(m => {
      foundry.packages.tagPackageAvailability(m);
      const deps = (m.data?.dependencies ?? []).reduce((arr, d) => {
        if ( d?.name ) arr.push(d.name);
        return arr;
      }, []);
      m.dependencies = deps.length ? deps : null;
      m.data.updated = this._updatedPackages.has(m.id);
      return m;
    }).sort((a, b) => a.data.title.localeCompare(b.data.title));

    // Prepare Worlds
    const worlds = this.worlds.map(w => {
      w.shortDesc = TextEditor.previewHTML(w.data.description);
      w.system = game.systems.get(w.data.system);
      foundry.packages.tagPackageAvailability(w);
      w.data.updated = this._updatedPackages.has(w.id);
      return w;
    }).sort((a, b) => a.data.title.localeCompare(b.data.title));

    systems.concat(modules).forEach(p => {
      p.authors = p.data.author;
      const singular = [0, 1, undefined].includes(p.data.authors?.length);
      p.labels = {authors: game.i18n.localize(`Author${singular ? "" : "Pl"}`)};
      if ( p.data.authors?.length ) p.authors = p.data.authors.map(a => {
        if ( a.url ) return `<a href="${a.url}" target="_blank">${a.name}</a>`;
        return a.name;
      }).join(", ");
    });

    // Return data for rendering
    const coreVersion = game.version;
    const versionDisplay = game.release.display;
    const canReachInternet = game.data.addresses.remote;
    const couldReachWebsite = game.data.coreUpdate.couldReachWebsite;
    return {
      coreVersion: coreVersion,
      release: game.release,
      coreVersionHint: game.i18n.format("SETUP.CoreVersionHint", {versionDisplay}),
      noSystems: !systems.length,
      systems: systems,
      modules: modules,
      worlds: worlds,
      languages: game.data.languages,
      options: options,
      adminKey: game.data.passwordString,
      updateChannels: Object.entries(CONST.SOFTWARE_UPDATE_CHANNELS).reduce((obj, c) => {
        obj[c[0]] = game.i18n.localize(c[1]);
        return obj;
      }, {}),
      updateChannelHints: Object.entries(CONST.SOFTWARE_UPDATE_CHANNELS).reduce((obj, c) => {
        obj[c[0]] = game.i18n.localize(c[1]+ "Hint");
        return obj;
      }, {}),
      coreUpdate: game.data.coreUpdate.hasUpdate ? game.i18n.format("SETUP.UpdateAvailable", game.data.coreUpdate) : false,
      canReachInternet: canReachInternet,
      couldReachWebsite: couldReachWebsite,
      slowResponse: game.data.coreUpdate.slowResponse,
      updateButtonEnabled: canReachInternet && couldReachWebsite
    };
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _renderInner(...args) {
    await loadTemplates(["templates/setup/parts/package-tags.html"]);
    return super._renderInner(...args);
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Confirm Admin Changes
    html.find("button#admin-save").click(this._onSaveAdmin.bind(this));

    // Create or Edit World
    html.find("button#create-world, button.edit-world").click(this._onWorldConfig.bind(this));

    // Generic Submission Button
    html.find('button[data-action]').click(this._onActionButton.bind(this));

    // Install Package
    html.find("button.install-package").click(this._onInstallPackageDialog.bind(this));

    // Update Package
    html.find("button.update").click(this._onUpdatePackage.bind(this));

    // Update All Packages
    html.find("button.update-packages").click(this._onUpdatePackages.bind(this));

    // Uninstall Package
    html.find("button.uninstall").click(this._onUninstallPackage.bind(this));

    // Update Core
    html.find("button#update-core").click(this._onCoreUpdate.bind(this));

    // Lock
    html.find("button.lock-toggle").click(this._onToggleLock.bind(this));

    html.find(`input[name=${this._tab}Filter]`).focus();

    html.find("a.system-install").click(this._onClickSystemInstall.bind(this));
  }

  /* -------------------------------------------- */

  /**
   * Post the setup configuration form
   * @param {Object} data
   * @return {Promise}
   * @private
   */
  async _post(data) {

    // Construct form data
    const formData = new FormDataExtended(this.form);
    for ( let [k, v] of Object.entries(data) ) {
      formData.set(k, v);
    }

    // Post the request and handle redirects
    try {
      const response = await fetchWithTimeout(Setup.setupURL, {method: "POST", body: formData});
      if (response.redirected) return window.location.href = response.url;

      // Process response
      const json = await response.json();
      if (json.error) {
        const message = game.i18n.localize(json.error);
        const err = new Error(message);
        err.stack = json.stack;
        throw err;
      }

      return json;
    }
    catch (e) {
      ui.notifications.error(e, {permanent: true});
      throw e;
    }
  }

  /* -------------------------------------------- */

  /**
   * Reload the setup view by re-acquiring setup data and re-rendering the form
   * @private
   */
  async reload() {
    this._progressButton = null;
    return Setup.getData(game.socket, game.view).then(setupData => {
      foundry.utils.mergeObject(game.data, setupData);
      foundry.utils.mergeObject(this, setupData);
      this.render();
      Object.values(ui.windows).forEach(app => {
        if ( app instanceof InstallPackage ) app.render();
      });
    });
  }

  /* -------------------------------------------- */

  /**
   * Generic button handler for the setup form which submits a POST request including any dataset on the button itself
   * @param {MouseEvent} event    The originating mouse click event
   * @return {Promise}
   * @private
   */
  async _onActionButton(event) {
    event.preventDefault();

    // Construct data to post
    const button = event.currentTarget;
    button.disabled = true;
    const data = duplicate(button.dataset);

    // Warn about world migration
    if ( data.action === "launchWorld" ) {
      const world = game.data.worlds.find(w => w.data.name === data.world);
      if ( !world ) return;
      if ( game.release.isGenerationalChange(world.data.coreVersion) ) {
        const confirm = await Dialog.confirm({
          title: game.i18n.localize("SETUP.WorldMigrationRequiredTitle"),
          content: game.i18n.format("SETUP.WorldMigrationRequired", {
            world: world.data.title,
            oldVersion: world.data.coreVersion,
            newVersion: game.release.display,
            nIncompatible: game.data.modules.filter(m => m.incompatible).length,
            nModules: game.data.modules.length
          }),
        });
        if ( !confirm ) return button.disabled = false;
      }
    }

    // Submit the post request
    const response = await this._post(data);
    button.disabled = false;
    return response;
  }

  /* -------------------------------------------- */

  /**
   * Confirm user intent when saving admin changes to the application configuration
   * @param {MouseEvent} event    The originating mouse click event
   * @return {Promise}
   * @private
   */
  async _onSaveAdmin(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    return Dialog.confirm({
      title: game.i18n.localize("SETUP.ConfigSave"),
      content: `<p class="notification">${game.i18n.localize("SETUP.ConfigSaveWarning")}</p>`,
      yes: async () => {
        await this._post({action: "adminConfigure"})
        this.element.html(`<p>${game.i18n.localize("SETUP.ConfigSaveRestart")}</p>`);
      },
      defaultYes: false
    });
  }

  /* -------------------------------------------- */

  /**
   * Begin creation of a new World using the config form
   * @param {MouseEvent} event    The originating mouse click event
   * @private
   */
  _onWorldConfig(event) {
    event.preventDefault();
    const button = event.currentTarget;
    let data = {};
    const options = {};
    if ( button.dataset.world ) {
      data = game.data.worlds.find(w => w.data.name === button.dataset.world);
    } else {
      if ( game.systems.size === 0 ) {
        ui.notifications.warn(game.i18n.localize("SETUP.YouMustInstallASystem"));
        return;
      }
      options.create = true;
    }
    new WorldConfig(data, options).render(true)
  }

  /* -------------------------------------------- */
  /*  Package Management                          */
  /* -------------------------------------------- */

  /**
   * Handle install button clicks to add new packages
   * @param {Event} event
   * @private
   */
  async _onInstallPackageDialog(event) {
    event.preventDefault();
    let button = this._progressButton = event.currentTarget;
    const list = button.closest(".tab").querySelector(".package-list");
    const type = list.dataset.packageType;
    new InstallPackage({packageType: type, setup: this}).render(true);
  }

  /* -------------------------------------------- */

  /**
   * Handle update button press for a single Package
   * @param {Event} event
   * @private
   */
  async _onUpdatePackage(event) {
    event.preventDefault();

    // Disable the button
    const button = event.currentTarget;

    let pack = this._getPackageFromButton(button);
    if ( !pack?.data?.manifest ) return;

    // Inquire with the server for updates
    if ( button.dataset.state === "check" ) {
      let data = await this._updateCheckOne(pack.type, pack.id, button);
      let manifest = data.manifest;

      if ( data.state === "error" ) {
        ui.notifications.error(data.error, {permanent: true});
        return;
      }

      if ( data.state === "warning" ) {
        ui.notifications.warn(data.warning);
        return;
      }

      if ( data.state === "trackChange" ) {
        if ( await this._promptTrackChange(pack, data.trackChange) ) {
          manifest = data.trackChange.manifest;
          data.state = "update";
        }
      }

      if ( data.state === "sidegrade" && ui.setup ) {
        await ui.setup.reload();
      }
      else if ( data.state === "update" ) {
        await this._updateDownloadOne(pack.type, pack.id, button, manifest);
        if ( ui.setup ) await ui.setup.reload();
      }
    }
  }

  /* -------------------------------------------- */

  /**
   * Prompt the user to use a new Package track it if they haven't previously declined.
   * @param {Object} pack          The local Package
   * @param {string} manifest      The local manifest url
   * @return {Promise<boolean>}    If the new track manifest should be used
   * @private
   */
  async _promptTrackChange(pack, trackChange) {
    let declinedManifestUpgrades = game.settings.get("core", "declinedManifestUpgrades");
    let declinedForVersion = declinedManifestUpgrades[pack.data.name] === pack.data.version;
    if ( declinedForVersion ) return false;

    // Display prompt
    let content = await renderTemplate("templates/setup/manifest-update.html", {
      localManifest: pack.data.manifest,
      localTitle: game.i18n.format("SETUP.PriorManifestUrl", {version: pack.data.version}),
      remoteManifest: trackChange.manifest,
      remoteTitle: game.i18n.format("SETUP.UpdatedManifestUrl", {version: trackChange.version}),
      package: pack.data.title
    });
    let accepted = await Dialog.confirm({
      title: `${pack.data.title} ${game.i18n.localize("SETUP.ManifestUpdate")}`,
      content: content,
      yes: () => {
        delete declinedManifestUpgrades[pack.data.name];
        return true;
      },
      no: () => {
        declinedManifestUpgrades[pack.data.name] = pack.data.version;
        return false;
      },
      defaultYes: true
    });
    game.settings.set("core", "declinedManifestUpgrades", declinedManifestUpgrades);
    return accepted;
  }


  /* -------------------------------------------- */

  /**
   * Traverses the HTML structure to find the Package this button belongs to
   * @param {HTMLElement} button  The clicked button
   * @returns {Promise<Object>} A Package
   * @private
   */
  _getPackageFromButton(button) {
    let li = button.closest("li.package");
    let id = li.dataset.packageId;
    let type = li.closest("ul.package-list").dataset.packageType;
    return game[`${type}s`].get(id, {strict: true});
  }

  /* -------------------------------------------- */

  /**
   * Execute upon an update check for a single Package
   * @param {string} type         The package type to check
   * @param {string} name         The package name to check
   * @param {HTMLElement} button  The update button for the package
   * @return {Promise<object>}    The status of the update check
   * @private
   */
  async _updateCheckOne(type, name, button) {
    button.disabled = true;

    // Get the update manifest from the server
    let manifest;
    const checkData = {
      type: type,
      name: name,
      button: button,
      state: ""
    };

    try {
      manifest = await Setup.checkPackage({type, name});
    } catch(err) {
      checkData.state = "error";
      checkData.error = game.i18n.localize("PACKAGE.UpdateCheckTimedOut");
      button.disabled = false;
      return checkData;
    }

    if ( manifest.error ) {
      checkData.state = "error";
      checkData.error = manifest.error;
      return checkData;
    }

    // Packages which cannot be updated because they require a more modern core version
    if ( !manifest.isSupported ) {
      button.innerHTML = `<i class="fas fa-ban"></i><label>${game.i18n.format("SETUP.PackageStatusBlocked")}</label>`;
      checkData.state = "warning";
      checkData.warning = game.i18n.format("SETUP.PackageUpdateBlocked", {
        name: manifest.name,
        vreq: manifest.minimumCoreVersion,
        vcur: game.version
      });
      return checkData;
    }

    // Packages which can be updated
    if ( manifest.isUpgrade ) {
      button.innerHTML = `<i class="fas fa-download"></i><label>${game.i18n.format("SETUP.PackageStatusUpdate")}</label>`;
      checkData.state = "update";
      checkData.manifest = manifest.manifest;
    }

    // Packages which are already current
    else {
      button.innerHTML = `<i class="fas fa-check"></i><label>${game.i18n.format("SETUP.PackageStatusCurrent")}</label>`;
    }

    if ( manifest.hasSidegraded ) {
      checkData.state = "sidegrade";
    }
    else if ( manifest.hasTrackChangeAvailable ) {
      checkData.state = "trackChange";
      checkData.trackChange = manifest.trackChange;
      checkData.manifest = manifest.manifest;
    }

    button.dataset.state = checkData.state;
    return checkData;
  }

  /* -------------------------------------------- */

  /**
   * Execute upon an update download for a single Package
   * Returns a Promise which resolves once the download has successfully started
   * @param {string} type         The package type to install
   * @param {string} name         The package name to install
   * @param {HTMLElement} button  The Download button
   * @return {Promise}
   * @private
   */
  async _updateDownloadOne(type, name, button, manifestUrl) {
    this._progressButton = button;
    this._progressButton.innerHTML = `<i class="fas fa-spinner fa-pulse"></i><label>${game.i18n.format("SETUP.PackageStatusUpdating")}</label>`;
    const manifest = await Setup.installPackage({type, name, manifest: manifestUrl}, data => {
      this.updateProgressBar(data);
      this.updateProgressButton(data);
    });
    this._updatedPackages.add(manifest.id);
    this._progressButton = null;
    return manifest;
  }

  /* -------------------------------------------- */

  /**
   * Handle uninstall button clicks to remove existing packages
   * @param {Event} event
   * @private
   */
  _onUninstallPackage(event) {
    event.preventDefault();

    // Disable the button
    let button = event.currentTarget;
    button.disabled = true;

    // Obtain the package metadata
    const li = button.closest(".package");
    const name = li.dataset.packageId;
    const type = li.closest(".package-list").dataset.packageType;

    // Get the target package
    let collection = game.data[type+"s"];
    let idx = collection.findIndex(p => p.id === name);
    let pack = collection[idx];

    // Define a warning message
    const title = pack.data.title;
    let warning = `<p>${game.i18n.format("SETUP.PackageDeleteConfirm", {type: type.titleCase(), title})}</p>`;
    // Based on https://stackoverflow.com/a/8084248
    const code = (Math.random() + 1).toString(36).substring(7, 11);
    if ( type === "world" ) {
      warning += `<p class="notification">${game.i18n.localize("SETUP.WorldDeleteConfirm1")}</p>`
      warning += `<p>${game.i18n.format("SETUP.WorldDeleteConfirm2")}<b>${code}</b></p>`;
      warning += `<p><input id="delete-confirm" type="text" required autocomplete="off"></p>`;
    } else {
      warning += `<p class="notification">${game.i18n.localize("SETUP.PackageDeleteNoUndo")}</p>`
    }

    // Confirm deletion request
    Dialog.confirm({
      title: game.i18n.format("SETUP.PackageDeleteTitle", {type: type.titleCase(), title}),
      content: warning,
      yes: async html => {

        // Confirm World deletion
        if ( type === "world" ) {
          const confirm = html.find("#delete-confirm").val();
          if ( confirm !== code ) {
            return ui.notifications.error("SETUP.PackageDeleteWorldConfirm", {localize: true});
          }
        }

        // Submit the server request
        const response = await Setup.uninstallPackage({type, name});
        if ( response.error ) {
          const err = new Error(response.error);
          err.stack = response.stack;
          ui.notifications.error(game.i18n.localize("SETUP.UninstallFailure") + ": " + err.message);
          console.error(err);
        } else {
          ui.notifications.info(`${type.titleCase()} ${name} ${game.i18n.localize("SETUP.UninstallSuccess")}.`);
          collection.splice(idx, 1);
        }

        // Re-render the setup form
        ui.setup.reload();
      }
    }).then(() => button.disabled = false);
  }

  /* -------------------------------------------- */

  /**
   * Execute upon an update-all workflow to update all packages of a certain type
   * @param {Event} event
   * @private
   */
  async _onUpdatePackages(event) {
    event.preventDefault();
    let button = event.currentTarget;
    button.disabled = true;
    const icon = button.querySelector("i");
    icon.className = "fas fa-spinner fa-pulse";
    let ol = $(".tab.active .package-list");
    let type = ol.data("packageType");

    // Get Packages
    let packages = [];
    ol.children(".package").each((i, li) => {
      const id = li.dataset.packageId;
      const pack = game[`${type}s`].get(id);
      if ( pack && pack.data.manifest && !pack.locked ) packages.push({
        id: id,
        status: "none",
        button: li.querySelector("button.update")
      });
    });

    // Ensure the package cache is warm
    await Setup.warmPackages({type});

    // Check for updates in parallel
    let shouldReload = false;
    const checks = [];
    for ( let [i, p] of packages.entries() ) {
      const check = this._updateCheckOne(type, p.id, p.button);
      checks.push(check);
      if (((i+1) % 10) === 0) await check; // Batch in groups of 10
    }
    const checkedPackages = await Promise.all(checks);

    // Execute updates one at a time
    let updateLog = [];
    for (let p of checkedPackages ) {
      const pack = game[`${p.type}s`].get(p.name);
      if ( p.state === "error" ) updateLog.push({package: pack, action: game.i18n.localize("Error"), actionClass: "fa-exclamation-circle", description: p.error});
      else if ( p.state === "warning" ) updateLog.push({package: pack, action: game.i18n.localize("Warning"), actionClass: "fa-exclamation-triangle", description: p.warning});
      if ( !(p.state === "update" || p.state === "trackChange") ) continue;
      if ( p.state === "sidegrade" ) shouldReload = true;
      let manifest = p.manifest;
      let shouldUpdate = true;
      if ( p.trackChange ) {
        shouldUpdate = await this._promptTrackChange(pack, p.trackChange);
        manifest = p.trackChange.manifest;
      }
      if ( shouldUpdate ) {
        try {
          let updated = await this._updateDownloadOne(type, p.name, p.button, manifest);
          if ( p.state !== "sidegrade" ) {
            updateLog.push({
              package: pack,
              action: game.i18n.localize("Update"),
              actionClass: "fa-check-circle",
              description: `${pack.data.version} ➞ ${updated.data.version}`
            });
            shouldReload = true;
          }
        }
        catch (exception) {
          updateLog.push({package: pack, action: game.i18n.localize("Error"), actionClass: "fa-exclamation-circle", description: exception.message});
        }
      }
      p.available = false;
    }

    // Display Updatelog
    if ( updateLog.length > 0 ) {
      let content = await renderTemplate("templates/setup/updated-packages.html", {
        changed: updateLog,
      });
      await Dialog.prompt({
        title: game.i18n.localize("SETUP.UpdatedPackages"),
        content: content,
        callback: () => {},
        options: {width: 600},
        rejectClose: false
      });
    }
    if (shouldReload && ui.setup) {
      await ui.setup.reload();
    }
    icon.className = "fas fa-cloud-download-alt";
    button.disabled = false;
  }

  /* -------------------------------------------- */

  /**
   * Handle lock button clicks to lock / unlock a Package
   * @param {Event} event
   * @private
   */
  async _onToggleLock(event) {
    event.preventDefault();

    // Submit a lock request and update package data
    const button = event.currentTarget;
    let pack = this._getPackageFromButton(button);
    let shouldLock = !pack.locked;
    await this._post({action: "lockPackage", type: pack.type, name: pack.id, shouldLock: shouldLock});
    pack.locked = shouldLock;

    // Update the setup interface
    let icon = button.querySelector(".fas");
    if (shouldLock) {
      button.classList.replace("lock", "unlock");
      icon.classList.replace("fa-unlock", "fa-lock");
    }
    else {
      button.classList.replace("unlock", "lock");
      icon.classList.replace("fa-lock", "fa-unlock");
    }
    let li = button.closest("li.package");
    let uninstall = li.querySelector(".uninstall");
    uninstall.hidden = shouldLock;
    let update = li.querySelector(".update");
    if ( update ) {
      update.hidden = shouldLock;
    }
  }

  /* -------------------------------------------- */

  /**
   * Spawn the system install dialog with a given system name already filled in.
   * @param {TriggeredEvent} event  The triggering event.
   * @private
   */
  _onClickSystemInstall(event) {
    event.preventDefault();
    const query = event.currentTarget.dataset.query;
    new InstallPackage({packageType: "system", setup: this, filterValue: query}).render(true);
  }

  /* -------------------------------------------- */
  /*  Core Software Update                        */
  /* -------------------------------------------- */

  /**
   * Handle button clicks to update the core VTT software
   * @param {Event} event
   * @private
   */
  async _onCoreUpdate(event) {
    const button = event.currentTarget;
    const form = button.form;
    const label = button.children[1];

    // Disable the form
    button.disabled = true;
    form.disabled = true;

    const progress = data => {
      if ( ["UpdateComplete", "Error"].includes(data.step) ) {
        // After the update has completed or was interrupted due to error, remove any listeners and update the UI
        // appropriately.
        Setup._removeProgressListener(progress);

        // Final form updates
        button.disabled = false;
        const icon = button.querySelector("i");
        icon.className = data.step === "UpdateComplete" ? "fas fa-check" : "fas fa-times";
        form.disabled = false;

        // Display a notification message
        const level = data.step === "UpdateComplete" ? "info" : "error";
        ui.notifications[level](data.message, {localize: true});
        return;
      }
      this.updateProgressBar(data);
      this.updateProgressButton(data);
    };

    // Condition next step based on action
    if ( button.value === "updateDownload" ) {
      this._progressButton = button;
      // Attach a listener to accept progress events from the server after the update process has begun.
      Setup._addProgressListener(progress);
    }

    // Post the update request
    const response = await this._post({action: button.value}).catch(err => {
      button.disabled = false;
      form.disabled = false;
      throw err;
    });

    if ( response.warn ) {
      button.disabled = false;
      form.disabled = false;
      return ui.notifications.warn(response.warn, {localize: true});
    }

    // Proceed to download step
    if ( button.value === "updateCheck" ) {
      let releaseData = new foundry.config.ReleaseData(response);
      ui.notifications.info(game.i18n.format("SETUP.UpdateInfoAvailable", {display: releaseData.display}));
      label.textContent = game.i18n.format("SETUP.UpdateButtonDownload", {display: releaseData.display});
      button.value = "updateDownload";
      button.disabled = false;
      if ( response.notes ) new UpdateNotes(response).render(true);
      if ( response.willDisableModules ) {
        ui.notifications.warn(game.i18n.format("SETUP.UpdateWarningWillDisable", {
          nIncompatible: game.data.modules.filter(m => m.incompatible).length,
          nModules: game.data.modules.length
        }), {permanent: true});
      }
    }
  }

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers               */
  /* -------------------------------------------- */

  /**
   * Update the display of an installation progress bar for a particular progress packet
   * @param {Object} data   The progress update data
   */
  updateProgressBar(data) {
    const tabName = data.type === "core" ? "update" : data.type+"s";
    const tab = this.element.find(`.tab[data-tab="${tabName}"]`);
    if ( !tab.hasClass("active") ) return;
    const progress = tab.find(".progress-bar");
    progress.css("visibility", "visible");

    // Update bar and label position
    let pl = `${data.pct}%`;
    let bar = progress.children(".bar");
    bar.css("width", pl);
    let barLabel = progress.children(".pct");
    barLabel.text(pl);
    barLabel.css("left", pl);
  }

  /* -------------------------------------------- */

  /**
   * Update installation progress for a particular button which triggered the action
   * @param {Object} data   The progress update data
   */
  updateProgressButton(data) {
    const button = this._progressButton;
    if ( !button ) return;
    button.disabled = data.pct < 100;

    // Update Icon
    const icon = button.querySelector("i");
    if ( data.pct < 100 ) icon.className = "fas fa-spinner fa-pulse";

    // Update label
    const label = button.querySelector("label");
    const step = game.i18n.localize(data.step);
    if ( label ) label.textContent = step;
    else button.textContent = " " + step;
  }
}

/**
 * The client side Updater application
 * This displays the progress of patching/update progress for the VTT
 * @type {Application}
 */
class UpdateNotes extends Application {
  constructor(target, options) {
    super(options);
    this.target = target;
    this.candidateReleaseData = new foundry.config.ReleaseData(this.target);
  }

  /* ----------------------------------------- */

  /** @override */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
	    id: "update-notes",
      template: "templates/setup/update-notes.html",
      width: 600
    });
  }

  /* ----------------------------------------- */

  /** @override */
  get title() {

    return `Update Notes - Foundry Virtual Tabletop ${this.candidateReleaseData.display}`;
  }

  /* ----------------------------------------- */

  /** @override */
  async getData(options) {
    return {
      notes: this.target.notes
    }
  }

  /* ----------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    html.find("button").click(ev => {
      ev.preventDefault();
      const button = ev.currentTarget;
      button.disabled = true;
      document.getElementById("update-core").click();
      button.innerHTML = `<i class="fas fa-spinner fa-spin"></i> Downloading`;
    });
  }
}

/**
 * The User Management setup application.
 * @extends {FormApplication}
 * @param {Users} object                      The {@link Users} object being configured.
 * @param {FormApplicationOptions} [options]  Application configuration options.
 */
class UserManagement extends FormApplication {

  /** @inheritdoc */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
	    id: "manage-players",
      classes: ["dark"],
      template: "templates/setup/user-management.html",
      popOut: false,
      closeOnSubmit: false,
      scrollY: ["#player-list"]
    });
  }

	/* -------------------------------------------- */

  /**
   * The template path used to render a single user entry in the configuration view
   * @type {string}
   */
  static USER_TEMPLATE = 'templates/setup/player-create.html';

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _render(...args) {
    await getTemplate(this.constructor.USER_TEMPLATE);
    return super._render(...args);
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    return {
      user: game.user,
      users: this.object.map(u => u.data),
      roles: UserManagement._getRoleLabels(),
      options: this.options,
      userTemplate: this.constructor.USER_TEMPLATE,
      passwordString: game.data.passwordString
    };
  }

  /* -------------------------------------------- */

  /**
   * Get a mapping of role IDs to labels that should be displayed
   * @private
   */
  static _getRoleLabels() {
    return Object.entries(CONST.USER_ROLES).reduce((obj, e) => {
      obj[e[1]] = game.i18n.localize(`USER.Role${e[0].titleCase()}`);
      return obj;
    }, {});
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);
    html.find('button[data-action]').click(UserManagement._onAction);
    html.find("input.password").keydown(UserManagement._onPasswordKeydown);
    html.find('label.show').click(UserManagement._onShowPassword);
    html.on('click', '.user-delete', UserManagement._onUserDelete);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _updateObject(event, formData) {

    // Construct updates array
    const userData = foundry.utils.expandObject(formData).users;
    const updates = Object.entries(userData).reduce((arr, e) => {
      const [id, userData] = e;
      userData._id = id;
      if ( userData.password === game.data.passwordString ) delete userData["password"];
      arr.push(userData);
      return arr;
    }, []);

    // The World must have at least one Gamemaster
    if ( !updates.some(u => u.role === CONST.USER_ROLES.GAMEMASTER) ) {
      return ui.notifications.error("USERS.NoGMError", {localize: true});
    }

    // Update all users and redirect
    return User.updateDocuments(updates, {diff: false}).catch(err => {
      ui.notifications.error(err);
    }).then(() => {
      ui.notifications.info("USERS.UpdateSuccess", {localize: true});
      return setTimeout(() => window.location.href = foundry.utils.getRoute("game"), 1000);
    })
  }

  /* -------------------------------------------- */

  /**
   * Handle new user creation event
   * @private
   */
  static async _onAction(event) {
    event.preventDefault();
    const button = event.currentTarget;
    button.disabled = true;
    switch ( button.dataset.action ) {
      case "create-user":
        await UserManagement._onUserCreate();
        break;
      case "configure-permissions":
        new PermissionConfig().render(true);
        break;
    }
    button.disabled = false;
  }

  /* -------------------------------------------- */

  /**
   * When the user enters some characters into a password field, present them with the "show" label that allows them
   * to see the text they have entered.
   * @param {KeyboardEvent} event     The initiating keydown event
   * @private
   */
  static _onPasswordKeydown(event) {
    if ( ["Shift", "Ctrl", "Alt", "Tab"].includes(event.key) ) return;
    const input = event.currentTarget;
    const show = input.nextElementSibling;
    show.classList.add("visible");
  }

  /* -------------------------------------------- */

  /**
   * Reveal the password that is being configured so the user can verify they have typed it correctly.
   * @param {PointerEvent} event        The initiating mouse click event
   * @private
   */
  static _onShowPassword(event) {
    const label = event.currentTarget;
    const group = label.closest(".form-group");
    const input = group.firstElementChild;
    input.type = input.type === "password" ? "text" : "password";
    label.classList.remove("active");
    if ( input.type === "text" ) label.classList.add("active");
  }

  /* -------------------------------------------- */

  /**
   * Handle creating a new User record in the form
   * @private
   */
  static async _onUserCreate() {

    // Create the new User
    let newPlayerIndex = game.users.size + 1;
    while ( game.users.getName(`Player${newPlayerIndex}` )) { newPlayerIndex++; }
    const user = await User.create({
      name: `Player${newPlayerIndex}`,
      role: CONST.USER_ROLES.PLAYER
    });

    // Render the User's HTML
    const html = await renderTemplate(UserManagement.USER_TEMPLATE, {
      user: user.data,
      roles: this._getRoleLabels()
    });

    // Append the player to the list and restore the button
    $("#player-list").append(html);
  }

  /* -------------------------------------------- */

  /**
   * Handle user deletion event
   * @private
   */
  static _onUserDelete(event) {
    event.preventDefault();
    let button = $(event.currentTarget),
      li = button.parents('.player'),
      user = game.users.get(li.attr("data-user-id"));

    // Craft a message
    let message = `<h4>${game.i18n.localize("AreYouSure")}</h4><p>${game.i18n.localize("USERS.DeleteWarning")}</p>`;
    if (user.isGM) message += `<p class="warning"><strong>${game.i18n.localize("USERS.DeleteGMWarning")}</strong></p>`;

    // Render a confirmation dialog
    new Dialog({
      title: `${game.i18n.localize("USERS.Delete")} ${user.name}?`,
      content: message,
      buttons: {
        yes: {
          icon: '<i class="fas fa-trash"></i>',
          label: game.i18n.localize("Delete"),
          callback: () => {
            user.delete();
            li.slideUp(200, () => li.remove());
          }
        },
        no: {
          icon: '<i class="fas fa-times"></i>',
          label: game.i18n.localize("Cancel")
        },
      },
      default: 'yes'
    }).render(true);
  }
}
